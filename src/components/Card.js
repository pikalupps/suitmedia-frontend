import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Card = ({ title, color, icon }) => {
  return (
    <div
      className="max-w-xs w-60 lg:w-full mx-2 shadow-lg rounded-lg overflow-hidden"
      style={{ backgroundColor: color }}
    >
      <div className="flex justify-center items-center my-2 sm:my-6 lg:my-10 px-6 py-4">
        <div className="text-center">
          <FontAwesomeIcon icon={icon} className="text-white text-lg sm:text-xl lg:text-2xl mx-auto" />
          <div className="mt-2 lg:mt-4">
            <p className="text-sm sm:text-base lg:text-xl mb-2 lg:mt-4 leading-tight text-white font-bold">{title}</p>
            <p className="text-xs lg:text-sm mb-2 lg:mt-4 leading-tight text-white">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris malesuada mi nec tristique pellentesque. Sed vitae convallis lacus, sed dignissim neque.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;