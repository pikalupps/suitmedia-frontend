import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import bgImage from '../images/bg.jpg';
import aboutBgImage from '../images/about-bg.jpg';

const Slider = () => {
  return (
    <Carousel showThumbs={false} infiniteLoop={true} showStatus={false}>
      <div className="relative">
        <img src={bgImage} alt="Image 1" />
        <div className="absolute top-3/4 left-1/2 transform -translate-x-3/4 -translate-y-1/2 text-left">
          <p className="text-white text-xs sm:text-lg lg:text-3xl font-bold bg-black bg-opacity-50 p-4 ml-2">THIS IS A PLACE WHERE TECHNOLOGY & CREATIVITY FUSED INTO DIGITAL CHEMISTRY</p>
        </div>
      </div>
      <div className="relative">
        <img src={aboutBgImage} alt="Image 2" />
        <div className="absolute top-3/4 left-1/2 transform -translate-x-3/4 -translate-y-1/2 text-left">
          <p className="text-white text-xs sm:text-lg lg:text-3xl font-bold bg-black bg-opacity-50 p-4 ml-2">WE DON'T HAVE THE BEST BUT WE HAVE THE GREATEST TEAM</p>
        </div>
      </div>
    </Carousel>
  );
};

export default Slider;
