import React from 'react';
import Card from './Card';
import { faLightbulb, faLandmark, faBalanceScale } from '@fortawesome/free-solid-svg-icons';

const CardList = () => {
  return (
    <div className="container mx-auto my-6 sm:my-24 lg:my-32">
      <h2 className="text-base sm:text-xl lg:text-3xl font-bold mb-4 text-center">OUR VALUES</h2>
      <div className="flex flex-wrap justify-center gap-2">
        <Card title="INNOVATIVE" color="#EA7C6F" icon={faLightbulb} />
        <Card title="LOYALTY" color="#6A996F" icon={faLandmark} />
        <Card title="RESPECT" color="#5696C2" icon={faBalanceScale} />
      </div>
    </div>
  );
};

export default CardList;