import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons';

const Footer = () => {
  return (
    <footer className="bg-gray-700 py-8">
      <div className="container mx-auto items-center justify-between px-4 text-center">
        <div className="text-white">
          <span>Copyright © 2016 PT. Company</span>
        </div>
        <div>
          <a href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={faFacebook} className="text-white mr-2" />
          </a>
          <a href="https://twitter.com/" target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={faTwitter} className="text-white" />
          </a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
