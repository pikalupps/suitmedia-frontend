import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const Contact = () => {
  const formik = useFormik({
    initialValues: {
      name: '',
      email: '',
      message: '',
    },
    validationSchema: Yup.object({
      name: Yup.string().required('Name is required'),
      email: Yup.string()
        .email('Invalid email format')
        .required('Email is required'),
      message: Yup.string().required('Message is required'),
    }),
    onSubmit: (values) => {
      console.log('Form is valid');
      console.log(values);
      formik.resetForm();
    },
  });

  return (
    <div className="container mx-auto w-full flex justify-center items-center mb-6 sm:mb-24 lg:mb-32">
      <div className="w-full max-w-xs sm:max-w-md md:max-w-lg">
        <h2 className="text-base sm:text-xl lg:text-3xl font-bold mb-4 text-center">CONTACT US</h2>
        <form onSubmit={formik.handleSubmit}>
          <div className="mb-4">
            <label htmlFor="name" className="block mb-2 text-sm lg:text-base">
              Name
            </label>
            <input
              type="text"
              id="name"
              name="name"
              value={formik.values.name}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              className={
                formik.touched.name && formik.errors.name
                  ? 'input-field border border-red-500 w-full text-sm lg:text-base lg:p-2 p-1'
                  : 'input-field border border-blue-500 w-full text-sm lg:text-base lg:p-2 p-1'
              }
            />
            {formik.touched.name && formik.errors.name && (
              <p className="error">{formik.errors.name}</p>
            )}
          </div>

          <div className="mb-4">
            <label htmlFor="email" className="block mb-2 text-sm lg:text-base">
              Email
            </label>
            <input
              type="email"
              id="email"
              name="email"
              value={formik.values.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              className={
                formik.touched.email && formik.errors.email
                  ? 'input-field border border-red-500 w-full text-sm p-1 lg:text-base lg:p-2'
                  : 'input-field border border-blue-500 w-full text-sm p-1 lg:text-base lg:p-2'
              }
            />
            {formik.touched.email && formik.errors.email && (
              <p className="error">{formik.errors.email}</p>
            )}
          </div>

          <div className="mb-4">
            <label htmlFor="message" className="block mb-2 text-sm lg:text-base">
              Message
            </label>
            <textarea
              id="message"
              name="message"
              value={formik.values.message}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              className={
                formik.touched.message && formik.errors.message
                  ? 'input-field border border-red-500 w-full text-sm p-1 lg:text-base lg:p-2'
                  : 'input-field border border-blue-500 w-full text-sm p-1 lg:text-base lg:p-2'
              }
            ></textarea>
            {formik.touched.message && formik.errors.message && (
              <p className="error">{formik.errors.message}</p>
            )}
          </div>

          <button
            type="submit"
            className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default Contact;
