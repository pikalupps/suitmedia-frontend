import React from 'react';
import Navbar from './components/Navbar';
import Slider from './components/Slider';
import CardList from './components/CardList';
import Contact from './components/Contact'
import Footer from './components/Footer';

const App = () => {
  return (
    <div>
      <div style={{ position: 'relative', zIndex: 2 }}>
        <Navbar />
      </div>
      <div style={{ position: 'relative', zIndex: 1 }}>
        <Slider />
      </div>
      <CardList />
      <Contact />
      <Footer />
    </div>
  );
};

export default App;